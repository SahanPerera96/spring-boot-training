package com.example.demo.controller;

import com.example.demo.common.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class TestRestController {

    @Value("${application.name}")
    private String applicationName;
    @Value("${application.model}")
    private String applicationModel;

    private Coach thisCoach;
    @GetMapping("/")
    public String helloWorld(){
        return applicationModel+" Hello World "+applicationName;
    }

    @Autowired
    public TestRestController(@Qualifier("tennisCoach") Coach coach){
        System.out.println("In Constructor "+ getClass().getSimpleName());
        thisCoach= coach;
    }

    @GetMapping("/getWorkOut")
    public String getWorkOut(){
        return thisCoach.getDailyWorkout();
    }

}
